import { Component, OnInit } from '@angular/core';
import { Noticia, Destacadas, Generales } from '../interfaces/noticia';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.scss']
})
export class InicioComponent implements OnInit {

  constructor() { }

  destacadas:Array<Noticia> = Destacadas;
  generales:Array<Noticia> = Generales;

  ngOnInit(): void {
  }

}

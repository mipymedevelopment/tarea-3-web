import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Noticia, Destacadas, Generales } from '../interfaces/noticia';

@Component({
  selector: 'app-articulo',
  templateUrl: './articulo.component.html',
  styleUrls: ['./articulo.component.scss']
})
export class ArticuloComponent implements OnInit {

  constructor(private _Activatedroute:ActivatedRoute) { }

  id:string|null|number = null;
  data:any = null;

  ngOnInit(): void {
    this._Activatedroute.paramMap.subscribe(params => { 
      this.id = params.get('id'); 
    });

    let data;
    data = Destacadas.find( item=>(
      item.id == this.id
    ));
    
    if(data==undefined){
      data = Generales.find( item=>(
        item.id == this.id
      ));
    }

    this.data = data;

  }
}

import { Component, OnInit, AfterViewInit } from '@angular/core';

@Component({
  selector: 'app-servicio',
  templateUrl: './servicio.component.html',
  styleUrls: ['./servicio.component.scss']
})
export class ServicioComponent implements AfterViewInit {

  constructor() { }

	message_sent:boolean = false;

	ngAfterViewInit(): void {
		'use strict';
        
		var form:any = document.getElementById('servicio-form');
        if(form){

			form.addEventListener('submit', function(event:any) {
				if (form.checkValidity() === false) {
					event.preventDefault();
					event.stopPropagation();
				}else{
					event.preventDefault();
					event.stopPropagation();
					this.message_sent = true;
				}
				form.classList.add('was-validated');
			}.bind(this), false);
		}
		
	}
}

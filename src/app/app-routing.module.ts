import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NoticiasComponent } from './noticias/noticias.component';
import { ServicioComponent } from './servicio/servicio.component';
import { InicioComponent } from './inicio/inicio.component';
import { ArticuloComponent } from './articulo/articulo.component';

const routes: Routes = [
  {path: '', redirectTo: 'inicio', pathMatch: 'full'},
  {path:'inicio', component:InicioComponent},
  {path:'noticias', component:NoticiasComponent},
  {path:'servicio', component: ServicioComponent},
  {path:'articulo/:id', component:ArticuloComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

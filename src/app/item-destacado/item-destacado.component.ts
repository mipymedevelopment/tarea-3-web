import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-item-destacado',
  templateUrl: './item-destacado.component.html',
  styleUrls: ['./item-destacado.component.scss']
})
export class ItemDestacadoComponent implements OnInit {

  constructor(private route: Router) { }

  @Input() data?:any;

  ngOnInit(): void {
  }

  onClick(){
    this.route.navigate([`/articulo/${this.data.id}`])
  }

}

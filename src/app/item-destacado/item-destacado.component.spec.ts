import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemDestacadoComponent } from './item-destacado.component';

describe('ItemDestacadoComponent', () => {
  let component: ItemDestacadoComponent;
  let fixture: ComponentFixture<ItemDestacadoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ItemDestacadoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemDestacadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

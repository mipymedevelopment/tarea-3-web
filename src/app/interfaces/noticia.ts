export interface Noticia {
    id:number;
    titulo:string;
    categoria:'entretenimiento'|'mundo'|'deporte'|'economia'|'salud'|'tecnologia';
    imagen:string;
    resumen:string;
}

export let Destacadas:Array<Noticia> = [
    {
        "id":1,
        "titulo": "Sony revela al estudio que llevará God of War a PC",
        "categoria": "entretenimiento",
        "resumen": "La llegada del GOTY, 2018 ha emocionado a los usuarios de la plataforma y la preventa ha sido un exito en steam",
        "imagen": "../../assets/gow.jpg"
    },
    {
        "id":2,
        "titulo": "La CEO de Warner prácticamente confirma 'Dune 2'",
        "categoria":"entretenimiento",
        "resumen":"Denis Villeneuve visionó Dune como un conjunto de dos películas, pero Warner no lo tenía tan claro y decidió esperar a conocer los resultados en taquilla de la primera parte.",
        "imagen": "../../assets/dune.jpg"
    },
    {
        "id":3,
        "titulo":"¿Realmente funcionan las cintas que miden las ondas cerebrales para mejorar el rendimiento?",
        "categoria":"tecnologia",
        "resumen":"Los atletas utilizan cada vez más dispositivos que miden las ondas cerebrales para mejorar su rendimiento. ¿Realmente funcionan?",
        "imagen":"../../assets/atletas.jpg"

    },
    {
        "id":4,
        "titulo":"No hay mucha diferencia entre la adicción a las drogas y al teléfono móvil",
        "categoria":"tecnologia",
        "resumen":"BBC Mundo entrevista al psicólogo español Marc Masip, quien defiende que 'el móvil es la heroína del siglo XXI'.",
        "imagen":"../../assets/drogas.jpg"
    }
]

export let Generales:Array<Noticia> = [
    {
        "id":5,
        "titulo":"Tokio: cuáles son los 5 nuevos deportes en los Juegos Olímpicos (y cuáles son los que vuelven)",
        "categoria":"deporte",
        "imagen":"../../assets/tokio.jpg",
        "resumen":"El Comité Olímpico Internacional agregó cinco deportes al programa de Tokio en un intento por atraer a un público más joven y reflejar 'la tendencia de urbanización del deporte'."
    },
    {
        "id":6,
        "titulo":"La dictadura cubana pidió hasta 25 años de prisión contra los manifestantes del 11J",
        "categoria": "mundo",
        "imagen":"../../assets/dictadura.jpg",
        "resumen":"Luego de las históricas marchas de julio, las fuerzas de seguridad de la dictadura detuvieron a cientos de personas. Ahora los fiscales del régimen reclaman penas extremadamente altas para muchas de las víctimas de la represión"
    },
    {
        "id":7,
        "titulo":"Continúa el explosivo aumento de cuentas en moneda extranjera en Chile",
        "categoria":"economia",
        "imagen": "../../assets/crisis.webp",
        "resumen": "Al menos desde el estallido social que varios lo vienen diciendo: los grandes patrimonios están sacando sus fortunas del país."
    },
    {
        "id":8,
        "titulo":"“Me siento fea”, “Me veo gordo”: pandemia aumentó la disconformidad con el cuerpo y en los casos más graves la dismorfia como trastorno mental",
        "categoria": "salud",
        "imagen":"../../assets/plandemia.jpg",
        "resumen":"No se trata solo de no querer verse al espejo o de evitar los encuentros sociales para que los demás no enjuicien su actual forma física. En los casos más graves hay una alteración del juicio de realidad que altera la vida de las personas."

    }
]
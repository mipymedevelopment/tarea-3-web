import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemGeneralComponent } from './item-general.component';

describe('ItemGeneralComponent', () => {
  let component: ItemGeneralComponent;
  let fixture: ComponentFixture<ItemGeneralComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ItemGeneralComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemGeneralComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

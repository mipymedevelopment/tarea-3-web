import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-item-general',
  templateUrl: './item-general.component.html',
  styleUrls: ['./item-general.component.scss']
})
export class ItemGeneralComponent implements OnInit {

  constructor(private route: Router) { }

  @Input() data?:any;

  ngOnInit(): void {
  }

  onClick(){
    this.route.navigate([`/articulo/${this.data.id}`])
  }

}

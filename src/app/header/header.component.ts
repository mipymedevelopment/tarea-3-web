import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(public route: Router) { }

  links = [
    { title: 'Inicio', url: 'inicio' },
    { title: 'Noticias', url: 'noticias' },
    { title: 'Servicio al cliente', url: 'servicio' }
  ]

  ngOnInit(): void {
  }
  
}

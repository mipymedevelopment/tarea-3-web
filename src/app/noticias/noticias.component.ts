import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Noticia, Generales, Destacadas } from '../interfaces/noticia';

@Component({
  selector: 'app-noticias',
  templateUrl: './noticias.component.html',
  styleUrls: ['./noticias.component.scss']
})
export class NoticiasComponent implements OnInit {

  constructor(private route: Router) { }

  destacadas:Array<Noticia> = Destacadas;
  generales:Array<Noticia> = Generales;

  ngOnInit(): void {
  }

  onClick(id:number){
    this.route.navigate([`/articulo/${id}`])
  }

}
